package method1;

import common.TextFile;
import common.WordOccurrences;
import interfaces.ObservableBagOfResult;

import java.util.concurrent.RecursiveAction;

public class TaskCountWordOccurrencesInFile extends RecursiveAction {

    //private static final int THRESHOLD_SIZE_LINE = 100;
    private TextFile file;
    private int fromLine;
    private int toLine;
    private int n;

    private ObservableBagOfResult mBagOfResult;

    public TaskCountWordOccurrencesInFile(ObservableBagOfResult bagOfResult, TextFile textFile, int fromLine, int toLine, int n){
        this.file = textFile;
        this.fromLine = fromLine;
        this.toLine = toLine;
        this.mBagOfResult = bagOfResult;
        this.n = n;
    }

    @Override
    protected void compute() {
        computeDirectly();
        /**
         *  divisione in sottotask per file di grandi dimensioni!
         *
            final int allLine = toLine - fromLine;
            if (allLine <= (THRESHOLD_SIZE_LINE)) {
            computeDirectly();
            return;
        }
        int split = allLine / 2;

        invokeAll(new TaskCountWordOccurrencesInFile(mRankingBagOfResult, file, fromLine, fromLine + split, n),
                new TaskCountWordOccurrencesInFile(mRankingBagOfResult, file, fromLine + split + 1, toLine,  n));*/
    }

    private void computeDirectly() {
        //l'aggiornamento avviene per parola piuttosto che per linea
        file.wordsLongerThanInStream(fromLine, toLine, n)
                .forEach(word -> mBagOfResult.updateTotalOccurrences(new WordOccurrences(word, 1)));
    }

}
