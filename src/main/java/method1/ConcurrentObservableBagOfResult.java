package method1;

import common.WordOccurrences;
import method2.RankingObservableBagOfResult;

import java.util.*;

//tengo una mappa con tutti i risultati di tutti i file e faccio un metodo che mi restituisce il k attualmente più grandi.

public class ConcurrentObservableBagOfResult extends RankingObservableBagOfResult {

    public ConcurrentObservableBagOfResult(final int numberOfResult){
        super(numberOfResult);
    }

    /**
     * metodo che, presa una mappa con le occorrenze di ogni parola (passata ad esempio dal un task che ha eseguito il calcolo su un determinato file),
     * aggiorna il numero di occorrenze totali delle parole trovate fino ad ora     *
     * @param partialOccurrences mappa che contiene le occorrenze relative ad una determinata computazione
     */
    public synchronized void updateTotalOccurrences(Map<String, Long> partialOccurrences){
        super.updateTotalOccurrences(partialOccurrences);
    }

    public synchronized void updateTotalOccurrences(WordOccurrences wordOccurrences){
        super.updateTotalOccurrences(wordOccurrences);
    }

}
