package method1;

import method2.RankingObservableBagOfResult;
import common.ObservableDocSetImpl;
import interfaces.CountOccurrencesService;
import interfaces.ObservableBagOfResult;
import interfaces.ObservableDocSet;
import view.ObserverView;

import javax.swing.*;

public class TestApproach1 {

    private static final int N_WORD_LENGHT = 4;
    private static final int K_RESULT = 6;

    public static void main(String[] args) {
        ObservableDocSet docSet = new ObservableDocSetImpl();
        ObservableBagOfResult observableBagOfResult = new ConcurrentObservableBagOfResult(K_RESULT);
        CountOccurrencesService service = new CountOccurrencesExecutorService(docSet, Runtime.getRuntime().availableProcessors()+1, (ConcurrentObservableBagOfResult) observableBagOfResult, N_WORD_LENGHT);
        SwingUtilities.invokeLater(()->{
            new ObserverView(service, docSet, (RankingObservableBagOfResult) observableBagOfResult).createAndShowGUI();
        });
    }
}
