package method1;

import common.ObservableDocSetImpl;
import common.TextFile;
import common.AbstractService;
import interfaces.ObservableDocSet;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

public class CountOccurrencesExecutorService extends AbstractService implements ObservableDocSetImpl.DocSetObserver{

    private ForkJoinPool mForkJoinPool;
    private Map<TextFile, Future> futureTask = new HashMap<>();//utile per la cancellazione dei task o lo stop durante la computazione

    public CountOccurrencesExecutorService(final ObservableDocSet observableDocSetImpl, final int nThread, final ConcurrentObservableBagOfResult bagOfResult, final int n){
        super(observableDocSetImpl, nThread, bagOfResult, n);
        this.mForkJoinPool = new ForkJoinPool(nThread);
    }


    @Override
    public void notifyAddDoc(TextFile textFile) {
        //eseguito dal thread grafico
        submitTask(textFile);
    }

    @Override
    public void notifyRemoveDoc(TextFile textFile) {
        //queste due azioni dentro la cancelTask non sono atomiche rispetto a quelle della submit task
        cancelTask(textFile);

    }


    private synchronized void submitTask(TextFile textFile) {
        futureTask.put(textFile, mForkJoinPool.submit(new TaskCountWordOccurrencesInFile((ConcurrentObservableBagOfResult) mObservableBagOfResult, textFile, 0, textFile.getNumberOfLines(), n)));
    }


    private synchronized void cancelTask(TextFile textFile) {
        futureTask.get(textFile).cancel(true);
        futureTask.remove(textFile);
    }


    @Override
    public void start() {
        //se ci sono già fai aggiunti al set faccio partire i task per i documenti che ho perso
        synchronized (this){
            observableDocSet.getSet().forEach(this::notifyAddDoc);
            if(this.observableDocSet != null){
                this.observableDocSet.attachObserver(this);
            }
        }
    }

    @Override
    public void stop() {
        synchronized (this) {
            this.observableDocSet.detachObserver(this);
            this.mForkJoinPool.shutdownNow();
            this.futureTask.forEach((file, future) -> future.cancel(true));
        }
    }

}
