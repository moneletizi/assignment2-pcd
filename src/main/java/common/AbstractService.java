package common;

import interfaces.ObservableBagOfResult;
import interfaces.CountOccurrencesService;
import interfaces.ObservableDocSet;

public abstract class AbstractService implements CountOccurrencesService {

    protected ObservableDocSet observableDocSet;
    protected ObservableBagOfResult mObservableBagOfResult;
    protected int n;
    protected int nThread;

    protected AbstractService(final ObservableDocSet observableDocSet, final int nThread, final ObservableBagOfResult observableBagOfResult, final int n){
        this.observableDocSet = observableDocSet;
        this.mObservableBagOfResult = observableBagOfResult;
        this.n = n;
        this.nThread = nThread;
    }

    //GETTER AND SETTER ARE NOT USEFUL AT THE MOMENT

    public abstract static class AbstractBuilder<T extends AbstractService, B extends ObservableBagOfResult> {

        private ObservableDocSetImpl mObservableDocSetImpl;
        private B mBagOfResult;
        private int n;
        private int nThread;

        public AbstractBuilder<T,B> documentSet(ObservableDocSetImpl observableDocSetImpl) {
            this.mObservableDocSetImpl = observableDocSetImpl;
            return this;
        }

        public AbstractBuilder<T,B> withNThread(int nThread) {
            this.nThread = nThread;
            return this;
        }

        public AbstractBuilder<T,B> withWordsLimit(int limit) {
            this.n = limit;
            return this;
        }

        public AbstractBuilder<T,B> withBagOfResult(B bagOfResult) {
            this.mBagOfResult = bagOfResult;
            return this;
        }

        public abstract T build();
    }


}
