package common;

import interfaces.ObservableDocSet;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

//monitor class
public class ObservableDocSetImpl implements ObservableDocSet {

    /*concurrent per evitare che nello stesso momento che qualcuno fa add ed è in notifyAddToAllObserver
        qualcun'altro faccia attachObserver
     */
    private ConcurrentLinkedQueue<DocSetObserver> mObserversList = new ConcurrentLinkedQueue<>();

    public interface DocSetObserver {
        void notifyAddDoc(TextFile textFile);
        void notifyRemoveDoc(TextFile textFile);
    }

    private Set<TextFile> mFiles = new HashSet<>();

    public synchronized boolean add(TextFile textFile) {
        boolean result =  mFiles.add(textFile);
        if(result)
            notifyAddToAllObserver(textFile);
        return result;
    }

    public synchronized boolean remove(TextFile textFile) {
        boolean result =  mFiles.add(textFile);
        if(result)
            notifyAddToAllObserver(textFile);
        return result;
    }

    public synchronized int getSize(){
        return mFiles.size();
    }

    public synchronized Set<TextFile> getSet(){
        return mFiles;
    }

    public void attachObserver(DocSetObserver observer){
        mObserversList.add(observer);
    }

    public void detachObserver(DocSetObserver observer){
        mObserversList.remove(observer);
    }

    private void notifyAddToAllObserver(TextFile textFile){
        notifyToAllObserver(o->o.notifyAddDoc(textFile));
    }
    
    private void notifyRemoveToAllObserver(TextFile textFile){
        notifyToAllObserver(o->o.notifyRemoveDoc(textFile));
    }

    private void notifyToAllObserver(Consumer<DocSetObserver> consumerFunction){
        mObserversList.forEach(consumerFunction);
    }

}