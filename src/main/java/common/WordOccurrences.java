package common;

import java.util.Comparator;
import java.util.Objects;

/**
 * classe i cui oggetti rappresentano il numero di occorrenze di una determinata parola.
 * é stata utile per implementare un TreeSet che si mantenesse ordinato a fronte di un'aggiunta di ogni parola
 */
public class WordOccurrences {

    private String word;
    private long counter;

    public static final Comparator<WordOccurrences> COMPARATOR = Comparator.comparing(WordOccurrences::getCounter)
            .thenComparing(WordOccurrences::getWord)
            .reversed();

    public WordOccurrences(String word, long counter) {
        this.word = word;
        this.counter = counter;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordOccurrences that = (WordOccurrences) o;
        return counter == that.counter &&
                Objects.equals(word, that.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, counter);
    }


    @Override
    public String toString() {
        return word + " : " + counter + "\n";
    }

}
