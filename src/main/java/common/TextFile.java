package common;

import interfaces.TextDocument;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextFile implements TextDocument {

    private int numberOfLines;
    private File file;

    private final static Function<String, Stream<String>> FROM_LINES_TO_WORD_FUNCTION = line -> Arrays.stream(line.trim().split("(\\s|\\p{Punct})+"));

    public TextFile(final File file) {
        this.numberOfLines = countNumberOfLines(file);
        this.file = file;
    }

    private int countNumberOfLines(File file) {
        int numberOfLines = 0;
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                numberOfLines++;
                line = reader.readLine();
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return numberOfLines;
    }

    public Stream<String> getLines(){
        try {
            return Files.lines(Paths.get(file.toURI()));
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    public Stream<String> wordsInStream(){
        return getLines().flatMap(FROM_LINES_TO_WORD_FUNCTION);
    }

    public Stream<String> wordsLongerThanInStream(int fromLine, int toLine, int lenght){
        return fromLinesToWordsStream(fromLine, toLine).filter(w->w.length()>lenght);
    }

    public Stream<String> fromLinesToWordsStream(int fromLine, int toLine) {
        return getLines().skip(fromLine).limit(toLine-fromLine).flatMap(FROM_LINES_TO_WORD_FUNCTION);
    }

    public int getNumberOfLines() {
        return numberOfLines;
    }

    public List<String> wordsIn() {
        return fromLinesToWordsStream(0, this.getNumberOfLines()).collect(Collectors.toList());

    }

    public List<String> wordsIn(int fromLine, int toLine){
        return fromLinesToWordsStream(fromLine, toLine).collect(Collectors.toList());
    }



    public boolean isBiggerThan(int lines){
        return numberOfLines>lines;
    }

    public File getFile(){
        return file;
    }

    @Override
    public String toString() {
        return file.getName();
    }
}
