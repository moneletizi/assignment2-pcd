package utils;

import io.vertx.core.Future;
import io.vertx.core.WorkerExecutor;

import java.util.concurrent.Callable;

public class VertxUtils {

    public static <T> Future<T> executeAsync(WorkerExecutor executor, Callable<T> callable) {
        Future<T> future = Future.future();
        executor.executeBlocking(event -> {
            try {
                T result = callable.call();
                event.complete(result);
            } catch (Exception e) {
                event.fail(e);
            }
        }, false, future);
        return future;
    }

    public static Future<Void> executeAsync(WorkerExecutor executor, Runnable runnable){
        Future<Void> future = Future.future();
        executor.executeBlocking(event -> {
            runnable.run();
            event.complete();
        }, false, future);
        return future;
    }

}
