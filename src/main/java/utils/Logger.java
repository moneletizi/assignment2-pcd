package utils;

public class Logger {
    public static final void log(String message){
        System.out.println(Thread.currentThread().getName() + ": " + message);
    }
}
