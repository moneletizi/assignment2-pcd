package utils;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {

    public final static Function<String, Stream<String>> FROM_LINES_TO_WORD_FUNCTION = line -> Arrays.stream(line.trim().split("(\\s|\\p{Punct})+"));

    public static Map<String, Long> countFilteredWordOccurrencesInLines(String line, Predicate<String> wordPredicate){
        return FROM_LINES_TO_WORD_FUNCTION.apply(line).filter(wordPredicate).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

}
