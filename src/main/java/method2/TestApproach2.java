package method2;

import common.ObservableDocSetImpl;
import interfaces.CountOccurrencesService;
import interfaces.ObservableBagOfResult;
import interfaces.ObservableDocSet;
import view.ObserverView;

import javax.swing.*;

public class TestApproach2  {

    private static final int N_WORD_LENGHT = 4;
    private static final int K_RESULT = 6;

    public static void main(String[] args) {
        ObservableDocSet docSet = new ObservableDocSetImpl();
        ObservableBagOfResult observableBagOfResult = new RankingObservableBagOfResult(K_RESULT);
        CountOccurrencesService service = new CountOccurrencesVertxService(docSet, Runtime.getRuntime().availableProcessors()+1, observableBagOfResult, N_WORD_LENGHT);
        SwingUtilities.invokeLater(()->{
            new ObserverView(service, docSet, (RankingObservableBagOfResult) observableBagOfResult).createAndShowGUI();
        });
    }
}
