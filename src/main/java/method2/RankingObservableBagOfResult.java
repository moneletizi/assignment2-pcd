package method2;

import common.WordOccurrences;
import interfaces.ObservableBagOfResult;

import java.util.*;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toCollection;

public class RankingObservableBagOfResult implements ObservableBagOfResult {

    private List<BagOfResultObserver> mObserversList = new ArrayList<>();

    public interface BagOfResultObserver{
        void onOccurrencesChanged(Set<WordOccurrences> newResult);
    }

    public RankingObservableBagOfResult(final int numberOfResult){
        this.numberOfResult = numberOfResult;
    }

    private int numberOfResult;

    //mappa per memorizzare i risultati finali (parola-numero_occorrenze)
    protected Map<String, Long> totalOccurrences = new HashMap<>();
    //set che si mantiene costantemente ordinato ad ogni aggiunta di ogni nuova parola
    private Set<WordOccurrences> mWordOccurrencesOrderedSet = new TreeSet<>(WordOccurrences.COMPARATOR);
    //memorizzo una copia del set notificato ogni volta alla GUI
    private Set<WordOccurrences> lastNotifiedSet = new TreeSet<>();


    /**
     * metodo che, presa una mappa con le occorrenze di ogni parola (passata ad esempio dal un task che ha eseguito il calcolo su un determinato file),
     * aggiorna il numero di occorrenze totali delle parole trovate fino ad ora     *
     * @param partialOccurrences mappa che contiene le occorrenze relative ad una determinata computazione
     */
    public void updateTotalOccurrences(Map<String, Long> partialOccurrences){
        partialOccurrences.forEach((k,v)->{
            long inseredValue = totalOccurrences.merge(k,v,(v1,v2)->v1+v2);
            replaceInSet(new WordOccurrences(k, inseredValue));
        });
        notifyIfRankedChanged();
    }

    public void updateTotalOccurrences(WordOccurrences wordOccurrences){
        final long countWord = totalOccurrences.getOrDefault(wordOccurrences.getWord(), 0L) + wordOccurrences.getCounter();
        totalOccurrences.put(wordOccurrences.getWord(),countWord);
        replaceInSet(new WordOccurrences(wordOccurrences.getWord(), countWord));
        notifyIfRankedChanged();
    }

    public synchronized void attachObserver(BagOfResultObserver observer) {
        mObserversList.add(observer);
    }


    public synchronized void detachObserver(BagOfResultObserver observer) {
        mObserversList.remove(observer);
    }


    private void replaceInSet(WordOccurrences wordOccurrences){
        mWordOccurrencesOrderedSet.removeIf(w->w.getWord().equals(wordOccurrences.getWord()));
        mWordOccurrencesOrderedSet.add(wordOccurrences);
    }


    private Set<WordOccurrences> getResult(int numberOfResult){
        return this.mWordOccurrencesOrderedSet.stream().limit(numberOfResult).collect(toCollection(()-> new TreeSet<>(WordOccurrences.COMPARATOR)));
    }

    private synchronized void notifyToAllObserver(Consumer<BagOfResultObserver> consumerFunction){
        mObserversList.forEach(consumerFunction);
    }


    private void notifyIfRankedChanged() {
        if (isRankedChanged()) {
            lastNotifiedSet = getResult(numberOfResult);
            notifyToAllObserver(o -> o.onOccurrencesChanged(lastNotifiedSet));
        }
    }

    private boolean isRankedChanged() {
        return !getResult(numberOfResult).equals(lastNotifiedSet);
    }


}
