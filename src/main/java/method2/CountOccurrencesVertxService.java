package method2;

import interfaces.CountOccurrencesService;
import interfaces.ObservableBagOfResult;
import interfaces.ObservableDocSet;
import io.vertx.core.*;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.parsetools.RecordParser;
import common.ObservableDocSetImpl;
import common.TextFile;
import utils.Utils;
import utils.VertxUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CountOccurrencesVertxService implements CountOccurrencesService {

    private Vertx mVertx;
    private WordCounterVerticle mWordCounterVerticle;

    public CountOccurrencesVertxService(final ObservableDocSet observableDocSetImpl, final int nThread, final ObservableBagOfResult rankingBagOfResult, final int n){
        mVertx = Vertx.vertx(new VertxOptions().setEventLoopPoolSize(1).setWorkerPoolSize(nThread));
        this.mWordCounterVerticle = new WordCounterVerticle(observableDocSetImpl, nThread, rankingBagOfResult, n);
    }


    //l'importante è che siano atomiche deploy e undeploy
    @Override
    public synchronized void start() {
        mVertx.deployVerticle(mWordCounterVerticle);
    }

    @Override
    public synchronized void stop() {
        mVertx.undeploy(mWordCounterVerticle.deploymentID());
    }


    private class WordCounterVerticle extends AbstractVerticle implements ObservableDocSetImpl.DocSetObserver {

        private WorkerExecutor executor;
        private int n;
        private ObservableBagOfResult mRankingBagOfResult;
        private ObservableDocSet mObservableDocSetImpl;
        private List<TextFile> promises = new ArrayList<>();


        public WordCounterVerticle(final ObservableDocSet observableDocSetImpl, final int nThread, final ObservableBagOfResult rankingBagOfResult, final int n){
            this.executor = mVertx.createSharedWorkerExecutor("pool", nThread);
            this.mObservableDocSetImpl = observableDocSetImpl;
            this.mRankingBagOfResult = rankingBagOfResult;
            this.n = n;
        }


        @Override
        public void start() throws Exception {
            super.start(); //chiamo la super dell'abstract verticle
            //n.b qui sono nell'event loop e non potrei fare operazioni bloccanti (monitor ecc.) quindi uso executeasync
            VertxUtils.executeAsync(executor, ()->{
                mObservableDocSetImpl.getSet().forEach(this::notifyAddDoc);
                if(this.mObservableDocSetImpl != null){
                    //questa  è syncronized. non devo farla nell'event loop. per questo uso execute async
                    this.mObservableDocSetImpl.attachObserver(this);
                }
            });

        }

        @Override
        public void stop() throws Exception {
            super.stop();
            //detach  è syncronized. non devo farla nell'event loop. per questo uso execute async
            VertxUtils.executeAsync(executor, ()-> mObservableDocSetImpl.detachObserver(this));
            executor.close();
            promises.clear();
        }

        @Override
        public void notifyAddDoc(TextFile textFile) {
            compute(textFile);
        }

        @Override
        public void notifyRemoveDoc(TextFile textFile) {
            //questo metodo viene eseguito da un flusso di controllo separato, quindi c è un momento in cui l event loop legge
            // la lista per capire se il file è stato rimosso. per questo uso runOnContext che fa si che il metodo venga eseguito nell event loop
            vertx.runOnContext(event -> promises.remove(textFile));
        }

        private void compute(TextFile textFile) {
            vertx.fileSystem().open(textFile.getFile().getPath(), new OpenOptions().setRead(true), handlerAsync -> {
                promises.add(textFile);
                //eseguito nell'event loop

                //tira su una linea alla volta a partire da un file asinconrono
                RecordParser.newDelimited("\n", handlerAsync.result()).handler(row->{
                    Future<Map<String, Long>> future;
                    //non è necessario che la lista di promesse sia syncronized perchè nell'handler sono sempre nell'event loop
                     if(promises.contains(textFile)){
                        future = VertxUtils.executeAsync(executor, () -> Utils.countFilteredWordOccurrencesInLines(row.toString(), w->w.length()>n));
                        //N.b se un file viene cancellato drante la cancellazione le occorrenze delle righe già computate sono già state visualizzate
                    }else{
                        future = Future.failedFuture("Canceled");
                    }
                    future.setHandler(event->{
                        if(event.succeeded() && promises.contains(textFile)){
                            mRankingBagOfResult.updateTotalOccurrences((event.result()));
                        }
                    });
                });
            });
        }


    }
}
