package view;

import interfaces.CountOccurrencesService;
import interfaces.ObservableBagOfResult;
import interfaces.ObservableDocSet;
import view.panel.AbstractServicePanel;
import view.panel.DocumentManagerPanel;

import javax.swing.*;

public abstract class View{

    //la view funge da osservatore della bag of result; quando cambiano i risultati a questa vengono notificate le prime k posizioni e di conseguenza le visualizza

    protected DocumentManagerPanel mDocumentManagerPanel;
    protected AbstractServicePanel mServiceManagerPanel;
    protected ObservableDocSet documentSet;

    protected View(CountOccurrencesService countOccurrencesService, ObservableDocSet observableDocSetImpl, ObservableBagOfResult rankingBagOfResult) {
        this.documentSet = observableDocSetImpl;
        this.mDocumentManagerPanel = new DocumentManagerPanel(documentSet);
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    public void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Count Word Occurrences System");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel container = new JPanel();
        container.add(mDocumentManagerPanel);
        container.add(mServiceManagerPanel);
        //Add content to the window.
        frame.add(container);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }



}

