package view.panel;

import common.ObservableDocSetImpl;
import common.TextFile;
import interfaces.ObservableDocSet;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class DocumentManagerPanel extends JPanel implements ActionListener, ObservableDocSetImpl.DocSetObserver {

    private JButton openFile, deleteFile;
    private JFileChooser fc;
    private JList<TextFile> fileNameList;
    private DefaultListModel<TextFile> textFileModel = new DefaultListModel<>();

    private ObservableDocSet mObservableDocSetImpl;

    public DocumentManagerPanel(ObservableDocSet observableDocSetImpl){
        super(new BorderLayout());
        this.mObservableDocSetImpl = observableDocSetImpl;
        if(observableDocSetImpl !=null){
            observableDocSetImpl.attachObserver(this);
        }
        fileNameList = new JList<>(textFileModel);
        //Create a file chooser
        fc = new JFileChooser();
        fc.setMultiSelectionEnabled(true);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        fc.setFileFilter(filter);
        JScrollPane resultScrollPane = new JScrollPane(fileNameList);

        openFile = new JButton("Open a File...");
        openFile.addActionListener(this);

        deleteFile = new JButton("Delete selected file");
        deleteFile.addActionListener(this);

        //For layout purposes, put the buttons in a separate panel
        JPanel buttonPanel = new JPanel(); //use FlowLayout
        buttonPanel.add(openFile);
        buttonPanel.add(deleteFile);

        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(resultScrollPane, BorderLayout.CENTER);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == openFile) {
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                TextFile textFile = new TextFile(file);
                mObservableDocSetImpl.add(textFile);
            }
        } else if(e.getSource() == deleteFile){
            mObservableDocSetImpl.remove(fileNameList.getSelectedValue());
        }
    }

    @Override
    public void notifyAddDoc(TextFile textFile) {
        SwingUtilities.invokeLater(()->textFileModel.addElement(textFile));
    }

    @Override
    public void notifyRemoveDoc(TextFile textFile) {
        SwingUtilities.invokeLater(()->textFileModel.removeElement(textFile));
    }
}