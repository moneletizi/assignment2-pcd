package view.panel;

import interfaces.CountOccurrencesService;
import method2.RankingObservableBagOfResult;
import common.WordOccurrences;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

public abstract class AbstractServicePanel extends JPanel implements ActionListener {

    protected JButton startComputation, stopComputation;
    protected JTextArea resultTextArea;
    protected RankingObservableBagOfResult mRankingBagOfResult;
    protected CountOccurrencesService mCountOccurrencesService;

    public AbstractServicePanel(CountOccurrencesService countOccurrencesService, RankingObservableBagOfResult rankingBagOfResult){
            super(new BorderLayout());
            mRankingBagOfResult = rankingBagOfResult;
            mCountOccurrencesService = countOccurrencesService;
            resultTextArea = new JTextArea(5,20);
            resultTextArea.setMargin(new Insets(5,5,5,5));
            resultTextArea.setEditable(false);
            JScrollPane resultScrollPane = new JScrollPane(resultTextArea);

            startComputation = new JButton("Start compute");
            startComputation.addActionListener(this);

            stopComputation = new JButton("Stop compute");
            stopComputation.addActionListener(this);


            //For layout purposes, put the buttons in a separate panel
            JPanel buttonPanel = new JPanel(); //use FlowLayout
            buttonPanel.add(startComputation);
            buttonPanel.add(stopComputation);

            //Add the buttons and the log to this panel.
            add(buttonPanel, BorderLayout.PAGE_START);
            add(resultScrollPane, BorderLayout.CENTER);

            observe();

        }

        public abstract void observe();

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == startComputation) {
                mCountOccurrencesService.start();
            } else if ( e.getSource() == stopComputation){
                mCountOccurrencesService.stop();
            }
        }


        protected void showResult(Set< WordOccurrences > resultMap){
            SwingUtilities.invokeLater(()->{
                resultTextArea.setText(""); //clear result
                resultMap.forEach(w->resultTextArea.append(w.toString()));//append result
            });
        }


}
