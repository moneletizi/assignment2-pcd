package view.panel;

import interfaces.CountOccurrencesService;
import method2.RankingObservableBagOfResult;
import common.WordOccurrences;

import java.util.Set;

public class ObserverServicePanel extends AbstractServicePanel implements RankingObservableBagOfResult.BagOfResultObserver {

    public ObserverServicePanel(CountOccurrencesService countOccurrencesService, RankingObservableBagOfResult rankingBagOfResult) {
        super(countOccurrencesService, rankingBagOfResult);
    }

    @Override
    public void onOccurrencesChanged(Set<WordOccurrences> newResult) {
        showResult(newResult);
    }

    @Override
    public void observe() {
        mRankingBagOfResult.attachObserver(this);
    }
}
