package view.panel;

import interfaces.CountOccurrencesService;
import method3.HotRankingObservableBagOfResult;

public class RxServicePanel extends AbstractServicePanel {

    public RxServicePanel(CountOccurrencesService countOccurrencesService, HotRankingObservableBagOfResult bagOfResult) {
        super(countOccurrencesService, bagOfResult);
    }

    @Override
    public void observe() {
        ((HotRankingObservableBagOfResult) mRankingBagOfResult).getSubject().
                onBackpressureLatest()
                .subscribe(this::showResult);
    }

}
