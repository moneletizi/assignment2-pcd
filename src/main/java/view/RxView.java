package view;

import method3.HotRankingObservableBagOfResult;
import interfaces.CountOccurrencesService;
import interfaces.ObservableDocSet;
import view.panel.RxServicePanel;

public class RxView extends View {

    public RxView(CountOccurrencesService countOccurrencesService, ObservableDocSet observableDocSetImpl, HotRankingObservableBagOfResult rankingBagOfResult) {
        super(countOccurrencesService, observableDocSetImpl, rankingBagOfResult);
        mServiceManagerPanel = new RxServicePanel(countOccurrencesService, rankingBagOfResult);
    }
}
