package view;

import method2.RankingObservableBagOfResult;
import interfaces.CountOccurrencesService;
import interfaces.ObservableDocSet;
import view.panel.ObserverServicePanel;

public class ObserverView extends View {

    public ObserverView(CountOccurrencesService countOccurrencesService, ObservableDocSet observableDocSetImpl, RankingObservableBagOfResult rankingBagOfResult) {
        super(countOccurrencesService, observableDocSetImpl, rankingBagOfResult);
        mServiceManagerPanel = new ObserverServicePanel(countOccurrencesService, rankingBagOfResult);

    }
}
