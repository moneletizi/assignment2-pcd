package method3;

import java.util.Iterator;
import java.util.stream.Stream;

public class MyStreamIterable<T> implements Iterable<T>{

    private Stream<T> mTStream;

    public MyStreamIterable(Stream<T> stream){
        mTStream = stream;
    }

    @Override
    public Iterator<T> iterator() {
        return mTStream.iterator();
    }
}