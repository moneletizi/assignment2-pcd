package method3;

import io.reactivex.processors.PublishProcessor;
import common.WordOccurrences;
import method2.RankingObservableBagOfResult;

import java.util.Set;

public class HotRankingObservableBagOfResult extends RankingObservableBagOfResult implements RankingObservableBagOfResult.BagOfResultObserver {

    private PublishProcessor<Set<WordOccurrences>> subject;

    public HotRankingObservableBagOfResult(int numberOfResult) {
        super(numberOfResult);
        subject = PublishProcessor.create();
        attachObserver(this);
    }

    public PublishProcessor<Set<WordOccurrences>> getSubject(){
        return subject;
    }

    @Override
    public void onOccurrencesChanged(Set<WordOccurrences> newResult) {
        subject.onNext(newResult);
    }

}
