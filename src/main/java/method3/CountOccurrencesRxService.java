package method3;

import interfaces.ObservableBagOfResult;
import interfaces.CountOccurrencesService;
import interfaces.ObservableDocSet;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import common.AbstractService;
import common.ObservableDocSetImpl;
import common.TextFile;
import utils.Utils;

import java.util.*;
import java.util.concurrent.Executors;

public class CountOccurrencesRxService extends AbstractService implements CountOccurrencesService, ObservableDocSetImpl.DocSetObserver {

    private Map<TextFile, Disposable> mTextFileDisposableMap = new HashMap<>();

    public CountOccurrencesRxService(ObservableDocSet observableDocSetImpl, int nThread, ObservableBagOfResult observableBagOfResult, int n) {
        super(observableDocSetImpl, nThread, observableBagOfResult, n);
    }

    @Override
    public synchronized void start() {
        observableDocSet.getSet().forEach(this::compute);
        if(this.observableDocSet != null){
            this.observableDocSet.attachObserver(this);
        }
    }

    @Override
    public synchronized void stop() {
        this.observableDocSet.detachObserver(this);
        mTextFileDisposableMap.values().forEach(Disposable::dispose);
        mTextFileDisposableMap.clear();
    }

    @Override
    public void notifyAddDoc(TextFile textFile) {
        compute(textFile);
    }

    private synchronized Disposable compute(TextFile textFile) {
        Disposable disposable = Flowable.just(textFile)
                .subscribeOn(Schedulers.io())
                .flatMap(file -> Flowable.fromIterable(new MyStreamIterable<>(file.getLines())))
                .flatMap(line->{
                    return Flowable.just(line)
                    .subscribeOn(Schedulers.from(Executors.newFixedThreadPool(nThread)))
                    .map(l-> Utils.countFilteredWordOccurrencesInLines(line, li->li.length()>n));
                }).observeOn(Schedulers.single())
                .subscribe(map-> mObservableBagOfResult.updateTotalOccurrences(map));
        mTextFileDisposableMap.put(textFile, disposable);
        return disposable;
    }

    @Override
    public void notifyRemoveDoc(TextFile textFile) {
        synchronized (this){
            Optional.ofNullable(mTextFileDisposableMap.remove(textFile)).ifPresent(Disposable::dispose);
        }
    }



}
