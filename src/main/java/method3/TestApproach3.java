package method3;

import common.ObservableDocSetImpl;
import interfaces.CountOccurrencesService;
import interfaces.ObservableBagOfResult;
import interfaces.ObservableDocSet;
import view.RxView;

import javax.swing.*;

public class TestApproach3 {
    private static final int N_WORD_LENGHT = 4;
    private static final int K_RESULT = 6;

    public static void main(String[] args) {
        ObservableDocSet docSet = new ObservableDocSetImpl();
        ObservableBagOfResult observableBagOfResult = new HotRankingObservableBagOfResult(K_RESULT);
        CountOccurrencesService service = new CountOccurrencesRxService(docSet, Runtime.getRuntime().availableProcessors()+1, observableBagOfResult, N_WORD_LENGHT);
        SwingUtilities.invokeLater(()->{
            new RxView(service, docSet, (HotRankingObservableBagOfResult) observableBagOfResult).createAndShowGUI();
        });
    }
}
