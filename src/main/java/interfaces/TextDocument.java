package interfaces;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

public interface TextDocument {
    Stream<String> getLines();
    Stream<String> wordsInStream();
    boolean isBiggerThan(int numberOfLine);
    int getNumberOfLines();
    List<String> wordsIn();
    List<String> wordsIn(int fromLine, int toLine);
    Stream<String> wordsLongerThanInStream(int fromLine, int toLine, int lenght);
    File getFile();

}
