package interfaces;

import common.WordOccurrences;

import java.util.Map;

public interface ObservableBagOfResult {
    void updateTotalOccurrences(Map<String, Long> partialOccurrences);
    void updateTotalOccurrences(WordOccurrences wordOccurrences);
}
