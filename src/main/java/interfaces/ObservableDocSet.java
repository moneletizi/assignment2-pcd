package interfaces;

import common.ObservableDocSetImpl;
import common.TextFile;

import java.util.Set;

public interface ObservableDocSet {
    boolean add(TextFile textFile);
    boolean remove(TextFile textFile);
    int getSize();
    Set<TextFile> getSet();
    void attachObserver(ObservableDocSetImpl.DocSetObserver observer);
    void detachObserver(ObservableDocSetImpl.DocSetObserver observer);
}
